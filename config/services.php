<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'linkedin' => [
        'client_id' => '86i9un89xnz3zz',
        'client_secret' => 'hCc6sMl9Dt1S980J',
        'redirect' => env('APP_URL', 'http://www.recommend.ninja') . '/login/linkedin/callback',
    ],

    'github' => [
        'client_id' => '86i9un89xnz3zz',
        'client_secret' => 'hCc6sMl9Dt1S980J',
        'redirect' => env('APP_URL', 'http://www.recommend.ninja') . '/login/github/callback',
    ],

    'facebook' => [
        'client_id' => '86i9un89xnz3zz',
        'client_secret' => 'hCc6sMl9Dt1S980J',
        'redirect' => env('APP_URL', 'http://www.recommend.ninja') . '/login/facebook/callback',
    ],

    'google' => [
        'client_id' => '86i9un89xnz3zz',
        'client_secret' => 'hCc6sMl9Dt1S980J',
        'redirect' => env('APP_URL', 'http://www.recommend.ninja') . '/login/google/callback',
    ],

    'twitter' => [
        'client_id' => '86i9un89xnz3zz',
        'client_secret' => 'hCc6sMl9Dt1S980J',
        'redirect' => env('APP_URL', 'http://www.recommend.ninja') . '/login/twitter/callback',
    ],

    'bitbucket' => [
        'client_id' => '86i9un89xnz3zz',
        'client_secret' => 'hCc6sMl9Dt1S980J',
        'redirect' => env('APP_URL', 'http://www.recommend.ninja') . '/login/twitter/callback',
    ],

];
