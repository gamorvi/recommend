<?php

namespace App\Http\Requests;

use Cache;
use Crypt;
use App\User;
use App\Http\Requests\Request;
use PragmaRX\Google2FA\Google2FA;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidatonFactory;

class Validate2faRequest extends FormRequest
{
    /**
     *
     * @var \App\User
     */
    private $user;
    private $google2fa;

    /**
     * Create a new FormRequest instance.
     *
     * @param \Illuminate\Validation\Factory $factory
     * @return void
     */
    public function __construct(ValidatonFactory $factory)
    {

        $this->google2fa = new Google2FA();

        $factory->extend(
            'valid_token',
            function ($attribute, $value, $parameters, $validator) {
                $secret = Crypt::decrypt($this->user->google2fa_secret);
                return $this->google2fa->verifyKey($secret, $value);
            },
            'The token you entered is not valid'
        );

        $factory->extend(
            'used_token',
            function ($attribute, $value, $parameters, $validator) {
                $key = $this->user->id . ':' . $value;

                return !Cache::has($key);
            },
            'You cannot reuse the same token'
        );

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

      try {
        $this->user = User::findOrFail(
            session('2fa:user:id')
        );

      } catch (Exception $exc) {
        return false;
      }

      return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'one_time_password' => 'bail|required|digits:6|valid_token|used_token',
        ];
    }

}
