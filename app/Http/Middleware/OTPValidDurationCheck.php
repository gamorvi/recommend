<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Carbon\Carbon;

class OTPValidDurationCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if ( ! \Auth::check() ) {
        $path = 'login';
      } else {
        $path = 'home';
      }

      $user = User::findOrFail(session('2fa:user:id'));

      // Get number of minutes user has been on OTP
      if (Carbon::parse($user->updated_at)->diffInMinutes(Carbon::now()) > 1) {
        $user->google2fa_code = null;
        $user->save();

        // If the user is logged in check if forced 2fa is being activated
        if ($user->enable_2fa) {
          $path = 'login';
        }

        return redirect()->route($path);
      }

      return $next($request);
    }
}
