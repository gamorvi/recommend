<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use App\JobProfile;

class CheckBasicInformation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
          $job_profile = JobProfile::where('user_id', Auth::user()->id)->first();
          if (!$job_profile) {
            return redirect()->route('job_profile.add');
          }
        }
        return $next($request);
    }
}
