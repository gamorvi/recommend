<?php

namespace App\Http\Controllers;

use Auth;
use App\Experience;
use Illuminate\Http\Request;

class ExperienceController extends Controller
{

    public function workHisory()
    {
      return view('experience.list');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $experiences = Experience::where('user_id', Auth::user()->id)->latest()->paginate(50);
      return response()->json($experiences);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'end_year' => 'required',
      ]);
      $experiences = Experience::create(array_merge($request->all(), ['user_id'=>Auth::user()->id]));
      return response()->json($experiences);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $experiences = Experience::where('user_id', Auth::user()->id)->find($id);
      return response()->json($experiences);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $experiences = Experience::where('user_id', Auth::user()->id)->find($id)->update($request->all());
      return response()->json($experiences);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Experience::find($id)->delete();
      return response()->json(['done']);
    }
}
