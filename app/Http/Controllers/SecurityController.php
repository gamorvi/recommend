<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use App\User;
use App\Audit;
use App\Mail\PasswordChanged;
use Illuminate\Http\Request;
use App\Rules\CurrentPasswordCheck;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;

class SecurityController extends Controller
{
    public function profile()
    {
      $user = User::findOrFail(Auth::user()->id);
      return view('security.profile', ['profile'=>'active'])->withUser($user);
    }

    public function storeBasicProfile(Request $request)
    {

        $request->validate([
          'firstname' => 'required|string|max:255',
          'lastname' => 'required|string|max:255',
          'othernames' => 'nullable|string|max:255',
          'contact_number' => 'nullable|string|max:15|regex:/(00)[0-9]{12}/',
        ]);

        $user = User::findOrFail(Auth::user()->id);
        $user->firstname = $request->firstname;
        $user->othernames = $request->othernames;
        $user->lastname = $request->lastname;
        $user->contact_number = $request->contact_number;

        if ($user->save()) {
          Session::flash('success', __('Your information was updated successfully'));
          return redirect()->route('profile');
        } else {
          Session::flash('danger', __('Sorry a problem occurred whilst updating your information'));
          return redirect()->route('profile');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function uploadPicture(Request $request)
    {

        $request->validate([
          'photo' => 'bail|required|mimes:jpeg,png|dimensions:max_width=500,min_height=800|max:2048'
        ]);

        $filename = str_random(8).time().'.'.$request->photo->getClientOriginalExtension();
        $request->photo->move(public_path('storage/profile/'. Auth::User()->id).'/', $filename);

        //$path = $request->photo->store('public');
        $user = User::findOrFail(Auth::user()->id);
        $old_logo = $user->photo;
        $user->photo = $filename;

        if ($user->save())
        {
          if ($old_logo != 'avatar.png') {
            $file_to_delete = 'storage/profile/'. Auth::User()->id . '/' . $old_logo;
            Storage::delete($file_to_delete);
          }

          Session::flash('success', __('Your profile picture was uploaded successfully'));
          return redirect()->route('profile');
        }
        else
        {
          Session::flash('danger', __('Sorry a problem occurred whilst uploading your profile picture'));
          return redirect()->route('profile');
        }
    }

    public function password()
    {
      return view('security.password', ['password'=>'active']);
    }

    public function two_factor()
    {
      return view('security.two_factor', ['two_factor'=>'active']);
    }

    public function accessLogs()
    {
      $user_id = Auth::User()->id;
      $accesses = Audit::where('user_id', $user_id)
                        //->where('path', '/login')
                        //->orWhere('path', '/auth/validate')
                        ->orderBy('created_at', 'DESC')
                        ->paginate(15);

      return view('security.access_log', ['access_logs'=>'active'])->withAccesses($accesses);
    }

    public function passwordChange(Request $request)
    {
      $request->validate([
        'current_password'=>['required', new CurrentPasswordCheck],
        'password' => ['required_with:password_confirmation', 'confirmed','min:8', 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/'],
      ]);

      $user = Auth()->User();
      $user->password = bcrypt($request->password);

      if ($user->save())
      {
        Mail::to($user->email)->send(new PasswordChanged($user->firstname));
        Session::flash('success', __('Your password was changed successfully.'));
        return redirect()->route('change.password');
      }
      else
      {
        Session::flash('danger', __('Oops! A problem occurred whilst updating your password.'));
        return redirect()->route('change.password');
      }
    }
}
