<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Crypt;
use Cache;
use App\User;
use App\Audit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PragmaRX\Google2FA\Google2FA;
use Illuminate\Foundation\Validation\ValidatesRequests;
use \ParagonIE\ConstantTime\Base32;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\Validate2faRequest;
use App\Mail\Enabled2fa;
use App\Mail\Disabled2fa;

// Log login of every user
use Jenssegers\Agent\Agent;


class Google2FAController extends Controller
{

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function enableTwoFactor(Request $request)
    {

      $user = Auth::User();

      $google2fa = new Google2FA();

      // Check if google2fa_code is not null and use it to create 2fa qr code
      if ($user->google2fa_code != null) {

        $secret = $user->google2fa_code;

      } else {

        //generate new secret
        $secret = $google2fa->generateSecretKey(32);

        //encrypt and then save secret
        $user->google2fa_code = $secret;
        $user->google2fa_secret = Crypt::encrypt($secret);

        $user->save();
      }

      //generate image for QR barcode
      $google2fa_url = $google2fa->getQRCodeGoogleUrl(
          config('app.name', 'Laravel'),
          $user->email,
          $secret
      );

      $request->session()->put('2fa:user:id', $user->id);

      return view('security.enableTwoFactor', ['image' => $google2fa_url,
          'secret' => $secret]);
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function verifyLogin2fa()
    {
      if (session('2fa:user:id'))
      {
        return view('security.validate', ['action'=>'login']);
      }

      return redirect(url()->current());
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function getValidateToken()
    {
      if (session('2fa:user:id'))
      {
        return view('security.validate', ['action'=>'validate']);
      }

      return redirect(url()->current());
    }

    /**
     *
     * @param \Illuminate\Http\Validate2faRequest $request
     * @return \Illuminate\Http\Response
     */
    public function loginWith2fa(Validate2faRequest $request)
    {
      //get user id and create cache key
       $userId = $request->session()->pull('2fa:user:id');
       $key    = $userId . ':' . $request->one_time_password;

       //use cache to store token to blacklist
       Cache::add($key, true, 4);

       //login and redirect user
       Auth::loginUsingId($userId);

       $agent = new Agent();

       Audit::create([
           'user_id'    => Auth::User()->id,
           'method'     => $request->getMethod(),
           'path' => $request->getPathInfo(),
           'query'      => $request->getQueryString(),
           'userAgent' => $agent->getUserAgent(),
           'ip'        => \Request::ip(),
           'device' => $agent->device(),
           'platform' => $agent->platform(),
           'browser' => $agent->browser(),
           'isDesktop' => $agent->isDesktop(),
           'isMobile' => $agent->isMobile(),
           'isPhone' => $agent->isPhone(),
           'isTablet' => $agent->isTablet()
       ]);

       return redirect()->route('home');
       //return redirect()->intended($this->redirectTo);
    }

    /**
     *
     * @param \Illuminate\Http\Validate2faRequest $request
     * @return \Illuminate\Http\Response
     */
    public function saveTwoFactor(Validate2faRequest $request)
    {

      $user = Auth::User();

      $user->enabled_2fa = 1;
      $user->google2fa_code = null;

      $user->save();

      Mail::to($user->email)->send(new Enabled2fa($user->firstname));

      Session::flash('success', __('Your Two-Factor Authentication (2FA) has been enabled successfully.'));
      return redirect()->route('two_factor');
    }

    /**
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function disableTwoFactorView()
    {

      if(is_null(Auth::User()->google2fa_secret) ) {
        return redirect()->route('two_factor');
      }

      $user = User::findOrFail(Auth::User()->id);

      Session::put('2fa:user:id', $user->id);

      $user->updated_at = Carbon::now();
      $user->save();

      return view('security.validate', ['action'=>'disable']);
    }

    /**
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function disable2fa(Validate2faRequest $request)
    {

      $user = $request->user();

      //make secret column blank
      $user->google2fa_secret = null;
      $user->google2fa_code = null;
      $user->enabled_2fa = 0;
      $user->save();

      Mail::to($user->email)->send(new Disabled2fa($user->firstname));

      Session::flash('success', __('Your Two Factor Authentication (2FA) has been disabled successfully.'));
      return redirect()->route('two_factor');
    }

    /**
     * Generate a secret key in Base32 format
     *
     * @return string
     */
    private function generateSecret()
    {
        $randomBytes = random_bytes(10);
        return Base32::encodeUpper($randomBytes);
    }

}
