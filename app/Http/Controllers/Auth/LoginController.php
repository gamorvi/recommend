<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use App\Audit;
use Socialite;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

// Log login of every user
use Jenssegers\Agent\Agent;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Send the post-authentication response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @return \Illuminate\Http\Response
     */
    protected function authenticated(Request $request, $user)
    {
      if ( $user->google2fa_secret && $user->enabled_2fa ) {

        // Set update_at in users table to limit OTP time
        $user->updated_at = Carbon::now();
        $user->save();
        $request->session()->put('2fa:user:id', $user->id);

        Auth::logout();

        return redirect()->route('login.validate');

      }

      // Otherwise proceed without 2FA validation
      $agent = new Agent();

      Audit::create([
          'user_id'    => \Auth::User()->id,
          'method'     => $request->getMethod(),
          'path' => $request->getPathInfo(),
          'query'      => $request->getQueryString(),
          'userAgent' => $agent->getUserAgent(),
          'ip'        => \Request::ip(),
          'device' => $agent->device(),
          'platform' => $agent->platform(),
          'browser' => $agent->browser(),
          'isDesktop' => $agent->isDesktop(),
          'isMobile' => $agent->isMobile(),
          'isPhone' => $agent->isPhone(),
          'isTablet' => $agent->isTablet()
      ]);

      return redirect()->intended($this->redirectTo);

    }

    /**
     *
     * Logout action
     */
    public function getLogout()
    {
      Auth::logout();
      return redirect(url('/'));
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($social)
    {
        return Socialite::driver($social)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($social)
    {
      try {

        $userSocial = Socialite::driver($social)->user();

        $user = User::where(['email'=>$userSocial->getEmail()])->first();

        if ( $user) {
          Auth::login($user);
          // Otherwise proceed without 2FA validation
          $agent = new Agent();

          Audit::create([
              'user_id' => \Auth::User()->id,
              'method'  => $request->getMethod(),
              'path' => $request->getPathInfo(),
              'query'  => $request->getQueryString(),
              'userAgent' => $agent->getUserAgent(),
              'ip' => \Request::ip(),
              'device' => $agent->device(),
              'platform' => $agent->platform(),
              'browser' => $agent->browser(),
              'isDesktop' => $agent->isDesktop(),
              'isMobile' => $agent->isMobile(),
              'isPhone' => $agent->isPhone(),
              'isTablet' => $agent->isTablet()
          ]);
          return redirect()->route('home');
        } else {
          $names = explode(' ', $userSocial->getName());
          $firstname = $names[0];
          $lastname = $names[count($names)-1];

          return view('auth.register',
            [
              'firstname'=>$firstname,
              'lastname'=>$lastname,
              'email'=>$userSocial->getEmail(),
              'password'=>'social_login'
            ]);

        }

      } catch (\Exception $e) {
        return redirect()->route('login');
      }

    }

}
