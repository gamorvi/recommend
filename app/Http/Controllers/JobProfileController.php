<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\JobType;
use App\Country;
use App\JobProfile;
use App\VisaStatus;
use App\NoticePeriod;
use App\EmploymentStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class JobProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

      $job_types = JobType::all();
      $visas = VisaStatus::all();
      $countries = Country::all();
      $user = User::findOrfail(Auth::user()->id);
      $notice_periods = NoticePeriod::all();
      $employment_statuses = EmploymentStatus::all();

      $job_profile = JobProfile::find(Auth::user()->id);

      return view('job_profile.edit', [
          'job_profile' => $job_profile,
          'job_types' => $job_types,
          'visas' => $visas,
          'countries' => $countries,
          'notice_periods' => $notice_periods,
          'employment_statuses' => $employment_statuses,
        ])->withUser($user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $job_types = JobType::all();
      $visas = VisaStatus::all();
      $countries = Country::all();
      $user = User::findOrfail(Auth::user()->id);
      $notice_periods = NoticePeriod::all();
      $employment_statuses = EmploymentStatus::all();

      $job_profile = JobProfile::find(Auth::user()->id);

      if ($job_profile) {
        return redirect()->route('job_profile.edit');
      }

      return view('job_profile.add', [
          'job_types' => $job_types,
          'visas' => $visas,
          'countries' => $countries,
          'notice_periods' => $notice_periods,
          'employment_statuses' => $employment_statuses,
        ])->withUser($user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $profile = JobProfile::where('user_id', Auth::user()->id)->first();

      if ($profile) {
        return redirect()->route('job_profile.edit');
      }

      $request->validate([
          "visa_status" => "required",
          "employment_status" => "required",
          "notice_period" => "required",
          "work_remote" => "required",
          "job_type" => "required",
          "relocate" => "required",
          'min_salary' => "required|max:10|regex:/^\d*(\.\d{1,2})?$/"
          // 'min_salary' => ['required_if:job_type,full_time', 'regex:/^\d*(\.\d{1,2})?$/'],
          // 'day_rate' => ['required_if:job_type,contract', 'regex:/^\d*(\.\d{1,2})?$/']
        ]);

        $profile = new JobProfile();
        $profile->user_id = Auth::user()->id;
        $profile->visa_status_id = $request->visa_status;
        $profile->country_code = $request->country;
        $profile->employment_status_id = $request->employment_status;
        $profile->notice_period_id = $request->notice_period;
        $profile->work_remote = $request->work_remote;
        $profile->job_type_id = $request->job_type;
        $profile->relocate = $request->relocate;
        $profile->salary_currency_code = 'GBP'; //$request->salary_currency_code;
        $profile->min_salary = $request->min_salary;

        if ($profile->save()) {
          Session::flash('success', __('Your details have been saved successfully'));
          return redirect()->route('job_profile.edit');
        } else {
          Session::flash('danger', __('Sorry a problem occurred whilst saving your details'));
          return redirect()->route('job_profile.add');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $request->validate([
          "visa_status" => "required",
          "employment_status" => "required",
          "notice_period" => "required",
          "work_remote" => "required",
          "job_type" => "required",
          "relocate" => "required",
          'min_salary' => "required|max:10|regex:/^\d*(\.\d{1,2})?$/"
          // 'min_salary' => ['required_if:job_type,full_time', 'regex:/^\d*(\.\d{1,2})?$/'],
          // 'day_rate' => ['required_if:job_type,contract', 'regex:/^\d*(\.\d{1,2})?$/']
        ]);

        $user = JobProfile::where('user_id', Auth::user()->id)->first();

        $profile = JobProfile::findOrfail($user->id);

        $profile->user_id = Auth::user()->id;
        $profile->visa_status_id = $request->visa_status;
        $profile->country_code = $request->country;
        $profile->employment_status_id = $request->employment_status;
        $profile->notice_period_id = $request->notice_period;
        $profile->work_remote = $request->work_remote;
        $profile->job_type_id = $request->job_type;
        $profile->relocate = $request->relocate;
        $profile->salary_currency_code = 'GBP'; //$request->salary_currency_code;
        $profile->min_salary = $request->min_salary;

        if ($profile->save()) {
          Session::flash('success', __('Your details have been saved successfully'));
          return redirect()->route('job_profile.edit');
        } else {
          Session::flash('danger', __('Sorry a problem occurred whilst saving your details'));
          return redirect()->route('job_profile.edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * {@inheritdoc}
     */
    protected function getUserByToken($token)
    {
        $response = $this->getHttpClient()->get(
           'https://api.linkedin.com/v1/people/~:(id,formatted-name,picture-url,email-address,public-profile-url)', [
            'headers' => [
                'Accept-Language' => 'en-US',
                'x-li-format'     => 'json',
                'Authorization'   => 'Bearer '.$token,
            ],
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * {@inheritdoc}
     */
    protected function mapUserToObject(array $user)
    {
        return (new User())->setRaw($user)->map([
            'id' => $user['id'], 'nickname' => null,
            'name' => $user['formattedName'], 'email' => $user['emailAddress'],
            'avatar' => array_get($user, 'pictureUrl'),
            'profileUrl' => array_get($user, 'publicProfileUrl'),
        ]);
    }
}
