$(document).ready(function() {

  var page = 1;
  var current_page = 1;
  var total_page = 0;
  var is_ajax_fire = 0;

  manageData();

  $('#create-item').on('shown.bs.modal', function () {
    $('#job_title').trigger('focus')
  })

  /* manage data list */
  function manageData() {
      $.ajax({
          dataType: 'json',
          url: url,
          data: {page:page}
      }).done(function(data){

      total_page = data.last_page;
      current_page = data.current_page;

      $('#pagination').twbsPagination({
        totalPages: total_page,
        visiblePages: current_page,
        onPageClick: function (event, pageL) {

          page = pageL;

          if(is_ajax_fire != 0){
            getPageData();
          }
        }
  	  });

      manageRow(data.data);

      is_ajax_fire = 1;

      });
  }

  $.ajaxSetup({
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
  });

  /* Get Page Data*/
  function getPageData() {

  	$.ajax({
        dataType: 'json',
      	url: url,
      	data: {page:page}
  	}).done(function(data){
  		manageRow(data.data);
  	});
  }

  /* Add new Post table row */
  function manageRow(data) {
  	var	rows = '';
  	$.each( data, function( key, value ) {
  	  	rows = rows + '<div data-id="'+value.id+'"></div>';
  	  	rows = rows + '<div>'+value.company_name+'</div>';
  	  	rows = rows + '<div>'+value.job_title+'</div>';
  	  	rows = rows + '<div>';
        rows = rows + '<button data-toggle="modal" data-target="#edit-item" class="btn btn-primary btn-xs edit-item"><i class="fa fa-edit"></i></button> ';
        rows = rows + '<button class="btn btn-danger  btn-xs remove-item"><i class="fa fa-trash"></i></button>';
        rows = rows + '</div>';
        rows = rows + '<br>';
  	});
  	$("#work-history").html(rows);
  }

  /* Create new Post */
  $(".crud-submit").click(function(e){
      e.preventDefault();
      var form_action = $("#create-item").find("form").attr("action");
      var start_month = $("#create-item").find("select#start_month option").filter(":selected").val();
      var start_year = $("#create-item").find("select#start_year option").filter(":selected").val();
      var company_name = $("#create-item").find("input[name='company_name']").val();
      var job_title = $("#create-item").find("input[name='job_title']").val();

      $.ajax({
          dataType: 'json',
          type:'POST',
          url: form_action,
          data:{start_month:start_month, start_year:start_year, company_name:company_name, job_title:job_title}
      }).done(function(data){
          getPageData();
          $("#create-item").modal('hide');
          toastr.success('Your work experience was added successfully.', 'Success', {timeOut: 3000, allowToastClose: true});
          $('body').removeClass('modal-open');
          $('.modal-backdrop').remove();
      });
  });

  /* Remove Post */
  $("body").on("click",".remove-item",function(){
      var id = $(this).parent("div").prev("div").prev("div").prev("div").data('id');
      var c_obj = $(this).parents("tr");

      $.ajax({
          dataType: 'json',
          type:'DELETE',
          url: url + '/' + id,
      }).done(function(data){

          c_obj.remove();
          toastr.success('Deleted successfully.', 'Success', {timeOut: 5000});
          getPageData();
      });

  });

  /* Edit Post */
  $("body").on("click",".edit-item",function(){
      var id = $(this).parent("div").prev("div").prev("div").prev("div").data('id');
      var job_title = $(this).parent("div").prev("div").prev("div").text();
      var company_name = $(this).parent("div").prev("div").text();
      $("#edit-item").find("input[name='job_title']").val(job_title);
      $("#edit-item").find("textarea[name='company_name']").val(company_name);
      $("#edit-item").find("form").attr("action",url + '/' + id);
  });

  /* Updated new Post */
  $(".crud-submit-edit").click(function(e){
      e.preventDefault();
      var form_action = $("#edit-item").find("form").attr("action");
      var job_title = $("#edit-item").find("input[name='job_title']").val();
      var company_name = $("#edit-item").find("textarea[name='company_name']").val();
      alert(form_action);
      $.ajax({
          dataType: 'json',
          type:'PUT',
          url: form_action,
          data:{job_title:job_title, company_name:company_name}
      }).done(function(data){

          getPageData();
          $(".modal").modal('hide');
          toastr.success('Updated successfully.', 'Success', {timeOut: 5000});

      });

  });
});
