$(document).ready(function() {

  $('.upload-user-image').on('dblclick', function() {
    $("#profile-picture").click();
  });

  $("#profile-picture").on("change", function() {
    $("#upload_avatar").submit();
  });

});
