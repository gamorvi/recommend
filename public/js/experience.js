$(document).ready(function() {

  getPageData();

  $('#create-item').on('shown.bs.modal', function () {
    $('#job_title').trigger('focus')
  })

  $('#current_job').change(function(){
      if(this.checked) {
        $("#optional").addClass("hidden");
        $("#create-item").find("select#start_month option").filter(":selected").val() = null;
        $("#create-item").find("select#start_year option").filter(":selected").val() = null;
      } else {
        $("#optional").removeClass("hidden");

      }
  });

  var months = [ "January", "February", "March", "April", "May", "June",
               "July", "August", "September", "October", "November", "December" ];

  $.ajaxSetup({
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
  });

  /* Get Page Data*/
  function getPageData() {

  	$.ajax({
        dataType: 'json',
      	url: url
  	}).done(function(data){

  		manageRow(data.data);

  	});

  }

  /* Add new Post table row */
  function manageRow(data) {
  	var	rows = '';
  	$.each( data, function( key, value ) {
        rows = rows + '<div id="history-'+value.id+'">';
  	  	rows = rows + '<div data-id="'+value.id+'"></div>';
  	  	rows = rows + '<div class="job_title">'+value.job_title+'</div>';
  	  	rows = rows + '<div class="company">'+value.company_name+'</div>';
        if (value.end_month == null || value.end_year == null) {
          var end = 'Present';
        } else {
          var end = months[value.end_month-1] + ' ' + value.end_year;
        }
        rows = rows + '<div class="duration">'+ months[value.start_month-1]+' ' +value.start_year + ' - ' + end + '</div>';
        rows = rows + '<div class="job_title">'+value.description+'</div>';
        rows = rows + '<div>';
        rows = rows + '<button data-toggle="modal" data-target="#edit-item" class="btn btn-primary btn-xs edit-item"><i class="fa fa-edit"></i></button> ';
        rows = rows + '<button class="btn btn-danger  btn-xs remove-item"><i class="fa fa-trash"></i></button>';
        rows = rows + '</div>';
        rows = rows + '<br>';
        rows = rows + '</div>';
  	});
  	$("#work-history").html(rows);
  }

  /* Create new Post */
  $(".crud-submit").click(function(e){
      e.preventDefault();
      $('#form').trigger("reset");
      var form_action = $("#create-item").find("form").attr("action");
      var start_month = $("#create-item").find("select#start_month option").filter(":selected").val();
      var start_year = $("#create-item").find("select#start_year option").filter(":selected").val();
      var end_month = $("#create-item").find("select#end_month option").filter(":selected").val();
      var end_year = $("#create-item").find("select#end_year option").filter(":selected").val();
      var company_name = $("#create-item").find("input[name='company_name']").val();
      var job_title = $("#create-item").find("input[name='job_title']").val();

      $.ajax({
          dataType: 'json',
          type:'POST',
          url: form_action,
          data:{start_month:start_month, start_year:start_year, company_name:company_name, job_title:job_title}
      }).done(function(data){
          getPageData();
          $(".modal").modal('hide');
          $('body').removeClass('modal-open');
          $('.modal-backdrop').remove();
          $(this).find('form')[0].reset();

          toastr.success('Your work experience was added successfully.', 'Success', {timeOut: 3000, allowToastClose: true});
      }).fail(function (jqXHR, textStatus) {
          alert(textStatus.message+' '+jqXHR.message);
      });
  });

  /* Remove Post */
  $("body").on("click",".remove-item",function(){
      var id = $(this).parent("div").prev("div").prev("div").prev("div").data('id');
      var div = $("div#history-"+id);

      $.ajax({
          dataType: 'json',
          type:'DELETE',
          url: url + '/' + id,
      }).done(function(data){

          div.remove();
          toastr.success('Deleted successfully.', 'Success', {timeOut: 5000});
          getPageData();

      });

  });

  /* Edit Post */
  $("body").on("click",".edit-item",function(){
      var id = $(this).parent("div").prev("div").prev("div").prev("div").prev("div").prev("div").data('id');
      var job_title = $(this).parent("div").prev("div").prev("div").prev("div").prev("div").text();
      var company_name = $(this).parent("div").prev("div").prev("div").prev("div").text();
      $("#edit-item").find("input[name='job_title']").val(job_title);
      $("#edit-item").find("textarea[name='company_name']").val(company_name);
      $("#edit-item").find("form").attr("action",url + '/' + id);
      alert(job_title); return;
  });

  /* Updated new Post */
  $(".crud-submit-edit").click(function(e){
      e.preventDefault();
      var form_action = $("#edit-item").find("form").attr("action");
      var job_title = $("#edit-item").find("input[name='job_title']").val();
      var company_name = $("#edit-item").find("textarea[name='company_name']").val();

      $.ajax({
          dataType: 'json',
          type:'PUT',
          url: form_action,
          data:{job_title:job_title, company_name:company_name}
      }).done(function(data){

          getPageData();
          $("#edit-item").modal('hide');
          $('body').removeClass('modal-open');
          $('.modal-backdrop').remove();
          toastr.success('Updated successfully.', 'Success', {timeOut: 5000});
      });

  });
});
