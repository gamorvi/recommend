<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountriesTable::class);
        $this->call(CurrencyTable::class);
        $this->call(EmploymentStatusesTable::class);
        $this->call(JobTypesTable::class);
        $this->call(NoticePeriodsTable::class);
        $this->call(VisaStatusTable::class);
    }
}
