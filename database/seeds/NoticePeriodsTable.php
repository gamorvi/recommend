<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class NoticePeriodsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $now = Carbon::now();

      DB::table('notice_periods')->truncate();

      DB::table('notice_periods')->insert([
        [
          'name' => 'Immediately',
          'created_at' => $now,
          'updated_at' => $now
        ],
        [
          'name' => '1 week',
          'created_at' => $now,
          'updated_at' => $now
        ],
        [
          'name' => '2 week',
          'created_at' => $now,
          'updated_at' => $now
        ],
        [
          'name' => '3 week',
          'created_at' => $now,
          'updated_at' => $now
        ],
        [
          'name' => '1 month',
          'created_at' => $now,
          'updated_at' => $now
        ],
        [
          'name' => '2 month',
          'created_at' => $now,
          'updated_at' => $now
        ],
        [
          'name' => '3 month',
          'created_at' => $now,
          'updated_at' => $now
        ],
        [
          'name' => '6 month',
          'created_at' => $now,
          'updated_at' => $now
        ]
      ]);
    }
}
