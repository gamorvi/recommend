<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class JobTypesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $now = Carbon::now();

      DB::table('job_types')->truncate();

      DB::table('job_types')->insert([
        [
          'name' => 'Full-time',
          'created_at' => $now,
          'updated_at' => $now
        ],
        [
          'name' => 'Contract',
          'created_at' => $now,
          'updated_at' => $now
        ],
        [
          'name' => 'Part-time',
          'created_at' => $now,
          'updated_at' => $now
        ]
      ]);
    }
}
