<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class VisaStatusTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $now = Carbon::now();

      DB::table('visa_statuses')->truncate();

      DB::table('visa_statuses')->insert([
          [
            'name' => 'British Citizen / National of EEA member state',
            'created_at' => $now,
            'updated_at' => $now
          ],
          [
            'name' => 'Swiss national',
            'created_at' => $now,
            'updated_at' => $now
          ],
          [
            'name' => 'Croatian national',
            'created_at' => $now,
            'updated_at' => $now
          ],
          [
            'name' => 'Tier 1 migrant (UK)',
            'created_at' => $now,
            'updated_at' => $now
          ],
          [
            'name' => 'Tier 2 migrant (UK)',
            'created_at' => $now,
            'updated_at' => $now
          ],
          [
            'name' => 'Tier 3 migrant (UK)',
            'created_at' => $now,
            'updated_at' => $now
          ],
          [
            'name' => 'Tier 4 migrant (UK)',
            'created_at' => $now,
            'updated_at' => $now
          ],
          [
            'name' => 'Tier 5 migrant (UK)',
            'created_at' => $now,
            'updated_at' => $now
          ],
          [
            'name' => 'Indefinite Leave to Remain/settlement (UK)',
            'created_at' => $now,
            'updated_at' => $now
          ],
          [
            'name' => 'Dependent of a EEA or Swiss national, or a Tier 1,2,4 or 5 migrant',
            'created_at' => $now,
            'updated_at' => $now
          ],
          [
            'name' => 'UK ancestry visa',
            'created_at' => $now,
            'updated_at' => $now
          ],
          [
            'name' => 'I do not have an UK visa',
            'created_at' => $now,
            'updated_at' => $now
          ],
          [
            'name' => 'None',
            'created_at' => $now,
            'updated_at' => $now
          ]
      ]);
    }
}
