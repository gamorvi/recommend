<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class EmploymentStatusesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $now = Carbon::now();

      DB::table('employment_statuses')->truncate();

      DB::table('employment_statuses')->insert([
        [
          'name' => 'Actively looking',
          'created_at' => $now,
          'updated_at' => $now
        ],
        [
          'name' => 'Currently interviewing',
          'created_at' => $now,
          'updated_at' => $now
        ],
        [
          'name' => 'Not started, but ready',
          'created_at' => $now,
          'updated_at' => $now
        ],
        [
          'name' => 'Not looking, but open to offers',
          'created_at' => $now,
          'updated_at' => $now
        ],
        [
          'name' => 'Not looking',
          'created_at' => $now,
          'updated_at' => $now
        ]
      ]);
    }
}
