<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->char('country_code', 2);
            $table->tinyInteger('relocate');
            $table->tinyInteger('work_remote');
            $table->integer('job_type_id');
            $table->char('salary_currency_code', 3)->nullable();
            $table->double('min_salary')->nullable();
            $table->char('day_rate_currency_code', 3)->nullable();
            $table->double('day_rate')->nullable();
            $table->integer('notice_period_id');
            $table->integer('employment_status_id');
            $table->integer('visa_status_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_profiles');
    }
}
