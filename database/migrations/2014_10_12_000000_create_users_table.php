<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('othernames')->nullable();
            $table->string('lastname');
            $table->string('contact_number')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('photo')->default('avatar.png')->nullable();
            $table->string('google2fa_code')->nullable();
            $table->text('google2fa_secret')->nullable();
            $table->tinyInteger('enabled_2fa')->default(0);
            $table->tinyInteger('verify_device_login')->default(0);
            $table->tinyInteger('account_type')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
