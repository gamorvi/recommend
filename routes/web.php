<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login/{social}','Auth\LoginController@redirectToProvider')->where('social','twitter|facebook|linkedin|google|github|bitbucket');
Route::get('/login/{social}/callback','Auth\LoginController@handleProviderCallback')->where('social','twitter|facebook|linkedin|google|github|bitbucket');

Route::get('/', 'Auth\RegisterController@showRegistrationForm')->name('register');

Auth::routes();
Route::get('logout', 'Auth\LoginController@getLogout')->name('logout');

Route::prefix('account')->middleware(['auth'])->group(function(){
  Route::get('/profile', 'JobProfileController@create')->name('job_profile.add');
  Route::post('/store', 'JobProfileController@store')->name('job_profile.store');

  Route::get('work-history', 'ExperienceController@workHisory');
  Route::resource('experience','ExperienceController');
});

// 2fa Route
Route::get('/2fa/verify', 'Google2FAController@verifyLogin2fa')->name('login.validate');
Route::post('/2fa/login', ['middleware'=>['throttle:3', '2faTimeCheck'], 'uses'=>'Google2FAController@loginWith2fa'])->name('2fa.login');

Route::get('/home', ['middleware'=>['auth', 'setup'], 'uses'=>'HomeController@index'])->name('home');

Route::prefix('settings')->middleware(['auth', 'setup'])->group(function() {

  Route::get('/my-profile', 'JobProfileController@edit')->name('job_profile.edit');
  Route::post('/update', 'JobProfileController@update')->name('job_profile.update');

  Route::get('/2fa/enable', 'Google2FAController@enableTwoFactor')->name('2fa.enable');
  Route::get('/2fa/verify', 'Google2FAController@getValidateToken')->name('2fa.validate');
  Route::post('/auth/verify', ['middleware'=>['throttle:3', '2faTimeCheck'], 'uses'=>'Google2FAController@saveTwoFactor'])->name('2fa.verify');
  Route::post('/2fa/confirm', ['middleware' => ['throttle:3', '2faTimeCheck'], 'uses' => 'Google2FAController@disable2fa'])->name('2fa.disable.verify');
  Route::get('/2fa/disable', 'Google2FAController@disableTwoFactorView')->name('2fa.disable');
  Route::get('/profile', 'SecurityController@profile')->name('profile');
  Route::get('/change-password', 'SecurityController@password')->name('change.password');
  Route::get('/2fa', 'SecurityController@two_factor')->name('two_factor');
  Route::post('/password/change', 'SecurityController@passwordChange')->name('user.change.password');
  Route::get('/access-logs', 'SecurityController@accessLogs')->name('access.logs');
  Route::post('/upload-avatar', 'SecurityController@uploadPicture')->name('upload.picture');
  Route::post('/update-basic', 'SecurityController@storeBasicProfile')->name('update.basic');

});
