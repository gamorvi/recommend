@extends('layouts.app')

@section('content')

<div class="row">

  <div class="col-md-3">
      @include('layouts.partials.my-account-menu')
  </div>

  <div class="col-md-9">

    <div class="col-md-12">

      <div class="card">
          <div class="card-header">{{__('Employment Profile')}}</div>

          <div class="card-body">
            @include('layouts.partials.flash-message')
            <form method="POST" action="{{ route('job_profile.update') }}">
                @csrf

                <div class="form-group row">
                    <label for="country" class="col-md-5 col-form-label text-md-right">{{ __('Where are you located?') }}</label>

                    <div class="col-md-7">
                        <select class="form-control" name="country" style="width: 100%;" required>
                          <option value="">{{__('Select one')}}</option>
                          @foreach ($countries as $row)
                            @if ($row->code == $job_profile->country_code)
                              <option value="{{$row->code}}" selected>{{$row->name}}</option>
                            @else
                              <option value="{{$row->code}}" >{{$row->name}}</option>
                            @endif
                          @endforeach
                        </select>

                    </div>
                </div>

                <div class="form-group row">
                    <label for="visa_status" class="col-md-5 col-form-label text-md-right">{{ __('What quilifies you to work in the UK?') }}</label>

                    <div class="col-md-7">
                        <select class="form-control" name="visa_status" style="width: 100%;" required>
                          <option value="">{{__('Select one')}}</option>
                          @foreach ($visas as $row)
                            @if ($row->id == $job_profile->visa_status_id)
                              <option value="{{$row->id}}" selected>{{$row->name}}</option>
                            @else
                              <option value="{{$row->id}}" >{{$row->name}}</option>
                            @endif
                          @endforeach
                        </select>

                    </div>
                </div>

                <div class="form-group row">
                    <label for="employment_status_id" class="col-md-5 col-form-label text-md-right">{{ __('What is your job search status?') }}</label>

                    <div class="col-md-7">
                        <select class="form-control" name="employment_status" style="width: 100%;" required>
                          <option value="">{{__('Select one')}}</option>
                          @foreach ($employment_statuses as $row)
                            @if ($row->id == $job_profile->employment_status_id)
                              <option value="{{$row->id}}" selected>{{$row->name}}</option>
                            @else
                              <option value="{{$row->id}}" >{{$row->name}}</option>
                            @endif
                          @endforeach
                        </select>

                    </div>
                </div>

                <div class="form-group row">
                    <label for="notice_period" class="col-md-5 col-form-label text-md-right">{{ __('What is your notice period?') }}</label>

                    <div class="col-md-7">

                        <select class="form-control" name="notice_period" style="width: 100%;" required>
                          <option value="">{{__('Select one')}}</option>
                          @foreach ($notice_periods as $row)
                            @if ($row->id == $job_profile->notice_period_id)
                              <option value="{{$row->id}}" selected>{{$row->name}}</option>
                            @else
                              <option value="{{$row->id}}" >{{$row->name}}</option>
                            @endif
                          @endforeach
                        </select>

                    </div>
                </div>

                <div class="form-group row">
                    <label for="work_remote" class="col-md-5 col-form-label text-md-right">{{ __('Are you interested in remote work?') }}</label>

                    <div class="col-md-7">
                        <select class="form-control" name="work_remote" style="width: 100%;" required>
                          <option value="">{{__('Select one')}}</option>

                            @if ($job_profile->work_remote==1)
                              <option value="1" selected>Yes</option>
                              <option value="0">No</option>
                            @endif
                            @if ($job_profile->work_remote==0)
                              <option value="1">Yes</option>
                              <option value="0" selected>No</option>
                            @endif

                        </select>

                    </div>
                </div>

                <div class="form-group row">
                    <label for="job_type" class="col-md-5 col-form-label text-md-right">{{ __('What job type(s) do you prefer?') }}</label>

                    <div class="col-md-7">
                        <select class="form-control" name="job_type" id="job_type" style="width: 100%;" required>
                          <option value="">{{__('Select one')}}</option>

                          @foreach ($job_types as $row)
                            @if ($row->id == $job_profile->job_type_id)
                              <option value="{{$row->id}}" selected>{{$row->name}}</option>
                            @else
                              <option value="{{$row->id}}" >{{$row->name}}</option>
                            @endif
                          @endforeach
                        </select>

                    </div>
                </div>

                <div class="form-group row" id="min_salary_div">
                    <label for="min_salary" class="col-md-5 col-form-label text-md-right">{{ __('What is your daily rate or salary expectation?') }}</label>

                    <div class="col-md-7">
                      <div class="row">
                        <div class="col-md-4">
                          <select class="form-control" name="relocate" style="width: 100%;">

                            <option value="GBP">GBP</option>

                          </select>
                        </div>
                        <div class="col-md-8">
                          <input id="min_salary" type="text" class="form-control{{ $errors->has('min_salary') ? ' is-invalid' : '' }}" name="min_salary" value="{{ $job_profile->min_salary }}">

                        </div>
                      </div>
                    </div>

                </div>

                <div class="form-group row">
                    <label for="relocate" class="col-md-5 col-form-label text-md-right">{{ __('Are you willing to relocate?') }}</label>

                    <div class="col-md-7">

                        <select class="form-control" name="relocate" style="width: 100%;" required>
                          <option value="">{{__('Select one')}}</option>

                          @if ($job_profile->relocate == '1')
                            <option value="1" selected>Yes</option>
                          @else
                            <option value="1">Yes</option>
                          @endif

                          @if ($job_profile->relocate == '0')
                            <option value="0" selected>No</option>
                          @else
                            <option value="0">No</option>
                          @endif

                        </select>

                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-7 offset-md-5">
                        <button type="submit" class="btn btn-success">
                            {{ __('Update') }}
                        </button>
                    </div>
                </div>
            </form>
          </div>
      </div>
    </div>

  </div>
</div>

@endsection
