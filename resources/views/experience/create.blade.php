<div class="modal fade" id="create-item" tabindex="-1" role="dialog" aria-labelledby="create-item-Label" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form data-toggle="validator" action="{{ route('experience.store') }}" method="POST">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Work Experience</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-close btn btn-outline-danger"></i></span>
        </button>
      </div>

      <div class="modal-body">


          <div class="form-group">
            <div class="material-switch">
              <input id="current_job" name="current_job" type="checkbox" value="1" />
              <label for="current_job" class="bg-success"></label> &nbsp;
              I currently work here
            </div>
          </div>

          <div class="form-group">
            <label class="control-label" for="job_title">Job Title</label>
            <input type="text" name="job_title" id="job_title" class="form-control" data-error="Please enter the position you occupied" required />
            <div class="help-block with-errors"></div>
          </div>

          <div class="form-group">
            <label class="control-label" for="company_name">Company</label>
            <input type="text" name="company_name" id="company_name" class="form-control" data-error="Please enter the position you occupied" required />
            <div class="help-block with-errors"></div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-md-6">
                <label class="control-label" for="company_name">From</label><br>
                  <select name="start_month" id="start_month" class="form-control" data-error="Please select the start month"  required>
                    <option value="">Start Month</option>
                    @for ($i = 1; $i <= 12; $i++)
                      <option value="{{ $i }}">{{ date('F', strtotime($i)) }}</option>
                    @endfor
                  </select>
                  <div class="help-block with-errors "></div>

                  <select name="start_year" id="start_year" class="form-control spacer" data-error="Please select the start year"  required>
                    <option value="">Start Year</option>
                    @for ($i = date('Y'); $i >= date('Y')-65; $i--)
                      <option value="{{ $i }}">{{ $i }}</option>
                    @endfor
                  </select>
                <div class="help-block with-errors "></div>
              </div>

              <div class="col-md-6" id="optional">
                <label class="control-label" for="company_name">To</label><br>
                  <select name="end_month" id="end_month" class="form-control" data-error="Please select the end month"  required>
                    <option value="">End Month</option>
                    @for ($i = 1; $i <= 12; $i++)
                      <option value="{{ $i }}">{{ date('F', strtotime($i)) }}</option>
                    @endfor
                  </select>
                  <div class="help-block with-errors "></div>

                  <select name="end_year" id="end_year" class="form-control spacer" data-error="Please select the end year"  required>
                    <option value="">End Year</option>
                    @for ($i = date('Y'); $i >= date('Y')-65; $i--)
                      <option value="{{ $i }}">{{ $i }}</option>
                    @endfor
                  </select>
                <div class="help-block with-errors "></div>
              </div>

            </div>
          </div>

          <div class="form-group">
            <label class="control-label" for="title">Description:</label>
            <textarea name="details" class="form-control" data-error="Please enter your job description"></textarea>
            <div class="help-block with-errors "></div>
          </div>

      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn crud-submit btn-success pull-right">Save</button>

      </div>
      </form>
    </div>
  </div>
</div>
