@extends('layouts.app')

@section('content')

<div class="row">

  <div class="col-md-3">
      @include('layouts.partials.my-account-menu')
  </div>

  <div class="col-md-9">

    <div class="col-md-12">

      <div class="card">
          <div class="card-header">{{__('Work Experience')}}</div>

          <div class="card-body">
            @include('layouts.partials.flash-message')

            <div class="progress">
              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 75%">25%</div>
            </div>

            <br>

            <div class="pull-right">
              <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#create-item">
                {{__("Add Work Experience")}}
              </button>
		        </div>
            <br>
            <div id="work-history">
            </div>

          </div>
      </div>
    </div>

  </div>
</div>

@include('experience.create')
@include('experience.edit')

@endsection
