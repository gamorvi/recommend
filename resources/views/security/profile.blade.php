@extends('layouts.app')

@section('content')

<div class="row">

  <div class="col-md-3">
      @include('layouts.partials.my-account-menu')
  </div>

  <div class="col-md-9">

    <div class="col-md-12">
      <div class="card">
          <div class="card-header">{{__('Profile Picture')}}</div>

          <div class="card-body">

            @include('layouts.partials.flash-message')

              <div class="col align-self-center text-center">
                <p>
                  {{__("To replace your profile picture double-click the image to upload a new one.")}}<br>
                  {{__("Maximum image dimensions: 500 x 800 px.")}}
                  {{__("Maximum image size: 2Mb.")}}
                </p>
                <div class="upload-user-image">
                  @if (!in_array(Auth::user()->photo, ["avatar.png", NULL, '', ' ']))
                    <img src="/storage/profile/{{Auth::user()->id}}/{{Auth::user()->photo}}" class="rounded" height="200" alt="{{ Auth::user()->firstname }}">
                  @else
                    <img src="/storage/profile/user.png" class="rounded" height="200" alt="{{ Auth::user()->firstname }}">
                  @endif
                </div>

                <br>

                <form id="upload_avatar" action="{{route('upload.picture', 1)}}" method="POST" enctype="multipart/form-data">

                  @csrf
                  <input type="file" name="photo" class="form-control-md" id="profile-picture" style="display:none">
                </form>
              </div>

          </div>
      </div>

      <br>

      <div class="card">
          <div class="card-header">{{__('Contact Information')}}</div>

          <div class="card-body">
            <form method="POST" action="{{ route('update.basic') }}">
                @csrf

                <div class="form-group row">
                    <label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

                    <div class="col-md-7">
                        <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{$user->firstname}}" required autofocus>

                        @if ($errors->has('firstname'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('firstname') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="othernames" class="col-md-4 col-form-label text-md-right">{{ __('Other Names') }}</label>

                    <div class="col-md-7">
                        <input id="othernames" type="text" class="form-control" name="othernames" value="{{ $user->othernames }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                    <div class="col-md-7">
                        <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ $user->lastname }}" required>

                        @if ($errors->has('lastname'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('lastname') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="contact_number" class="col-md-4 col-form-label text-md-right">{{ __('Contact Number') }}</label>

                    <div class="col-md-7">
                        <input id="contact_number" type="contact_number" class="form-control{{ $errors->has('contact_number') ? ' is-invalid' : '' }}" name="contact_number" value="{{ $user->contact_number }}" required>

                        @if ($errors->has('contact_number'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('contact_number') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-7 offset-md-4">
                        <button type="submit" class="btn btn-success">
                            {{ __('Update') }}
                        </button>
                    </div>
                </div>
            </form>
          </div>
      </div>
    </div>

  </div>
</div>

@endsection
