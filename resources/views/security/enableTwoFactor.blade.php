@extends('layouts.app')

@section('content')

      <div class="row">

        <div class="col align-self-center text-center">
          <p>
          {{__("Scan the QR code (image) below, using your Two-Factor Authentication app.")}}<br>
          {{__('E.g. Google Authenticator or Authy')}}
          </p>

          <div>
            <p>
              <img alt="2FA QR barcode" src="{{ $image }}" class="center-block" />
            </p>

            <code style="font-size:20px;">
              {{ $secret }}
            </code>
          </div>

        <form method="POST" action="{{route('2fa.verify')}}">
            {!! csrf_field() !!}

            <div class="form-group{{ $errors->has('one_time_password') ? ' has-error' : '' }}">
              <div class="col col-md">
                <input type="number" class="form-control-lg text-center" name="one_time_password" placeholder="Code e.g. 123456">
              </div>

              <div>
                  @if ($errors->has('one_time_password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('one_time_password') }}</strong>
                    </span>
                  @endif
              </div>
            </div>

            <input type="hidden" name="secret" value="{{$secret}}" />

            <div class="form-group last">
                    <button type="submit" class="btn btn-success btn-lg">
                        {{__('Proceed')}}
                    </button>
            </div>

        </form>
      </div>
    </div>

@endsection
