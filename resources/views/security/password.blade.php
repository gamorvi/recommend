@extends('layouts.app')

@section('content')

<div class="row">

  <div class="col-md-3">
      @include('layouts.partials.my-account-menu')
  </div>

  <div class="col-md-9">

    <div class="col-md-12">
      <div class="card">
          <div class="card-header">{{__('Change Password')}}</div>

          <div class="card-body">

            @include('layouts.partials.flash-message')

            <form class="form-header" method="POST" action="{{ route('user.change.password') }}">
            @csrf
            <div class="col-md-6">

            <div class="form-group{{ $errors->has('current_password') ? ' has-error' : '' }}">
              <label>{{__('Current Password')}}</label>
                <input placeholder="{{__('Please enter your current password')}}" id="current_password" type="password" class="form-control input-lg" name="current_password" required>

            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <label>{{__('New Password')}}</label>
              <input placeholder="{{__('Please enter your new password')}}" id="password" type="password" class="form-control input-lg" name="password" required>
              <div style="font-size:12px;">
                {{__("Your password must be at least 8 characters long, should contain at-least 1 uppercase, 1 lowercase, 1 numeric and 1 special character.")}}
              </div>
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
              <label>{{__('Confirm New Password')}}</label>
              <input placeholder="{{__('Please re-type your new password')}}" id="password-confirm" type="password" class="form-control input-lg" name="password_confirmation" required>

            </div>
            </div>

            <div class="col-md-6 form-group last">
              <button type="submit" class="btn btn-success">{{__('Change Password')}} </button>
            </div>

          </form>
        </div>
      </div>
  </div>
</div>

@endsection
