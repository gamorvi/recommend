@extends('layouts.app')

@section('content')

<div class="row">

  <div class="col-md-3">
      @include('layouts.partials.my-account-menu')
  </div>

  <div class="col-md-9">

    <div class="col-md-12">
      <div class="card">
          <div class="card-header">{{__('Access Log')}}</div>

          <div class="card-body">

            <div class="col-md-12">
            <p>
              {{__('Security starts with knowledge.')}}
              {{__('This page lists details about access to your account.')}}
              {{__('If you notice a problem, kindly prompt us and we will jump right on it.')}}
            </p>


            <table class="table table-striped">
              <thead>
                <tr>
                  <th>{{__('Date')}}</th>
                  <th>{{__('Time')}}</th>
                  <th>{{__('Device')}}</th>
                  <th>{{__('Platform')}}</th>
                  <th>{{__('Browser')}}</th>
                  <th>{{__('IP Address')}}</th>
                </tr>
              <tbody>
                @php
                  $i = 0;
                @endphp
                @foreach($accesses as $row)

                <tr>
                  <td>
                    {{$row['created_at']->format('D, jS M Y')}}
                  </td>
                  <td>
                    {{$row['created_at']->format('H:i:s')}}
                  </td>
                  <td>
                    @php
                      if($row['isMobile']==1 || $row['isPhone']==1):
                        $device = 'mobile';
                      elseif($row['isDesktop']==1 ):
                        $device = 'desktop';
                      else:
                        $device = 'tablet';
                      endif;
                    @endphp

                    <i class="fa fa-{{$device}}"></i>

                    @if($row['isMobile']==1 || $row['isPhone']==1)
                       {{__('Mobile Phone')}}
                    @elseif($row['isDesktop']==1 )
                      {{__('Desktop')}}
                    @else
                      {{__('Tablet')}}
                    @endif

                  </td>
                  <td>
                    @if(in_array($row['device'], ['Macintosh', 'iPhone']))
                      <i class="fa fa-apple"></i>
                    @elseif(in_array($row['device'], ['AndroidOS', 'Android']) || in_array($row['platform'], ['AndroidOS', 'Android']))
                      <i class="fa fa-android"></i>
                    @elseif($row['device']=='Windows')
                      <i class="fa fa-windows"></i>
                    @elseif($row['device']==0)
                      {{''}}
                    @else
                      {{$row['device']}}
                    @endif

                    {{$row['platform']}}
                  </td>
                  <td>
                    @php

                      $browser = strtolower($row['browser']);
                      $browser = ($browser != 'ie') ? $browser : 'internet-explorer';

                    @endphp

                    <i class="fa fa-{{$browser}}"></i> {{$row['browser']}}
                  </td>
                  <td>
                    {{$row['ip']}}
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            {{ $accesses->links() }}
          </div>
          </div>



        </div>
      </div>
  </div>
</div>

@endsection
