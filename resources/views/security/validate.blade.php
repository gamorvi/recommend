@extends('layouts.app')

@section('content')

  <div class="row">
    <div class="col align-self-center text-center">
      <p>
        {{__('Please enter the code on your Two-Factor Authentication application below.')}}
      </p>

    <form method="POST"
      @if ($action == 'login')
        action="{{route('2fa.login')}}"
      @elseif ($action == "validate")
        action="{{route('2fa.validate')}}"
      @else
        action="{{route('2fa.disable.verify')}}"
      @endif
    >
        @csrf

        <div class="form-group{{ $errors->has('one_time_password') ? ' has-error' : '' }}">
          <div class="col col-md">
            <input type="number" class="form-control-lg text-center" name="one_time_password" placeholder="Code e.g. 123456">
          </div>

          <div>
              @if ($errors->has('one_time_password'))
                <span class="help-block">
                    <strong>{{ $errors->first('one_time_password') }}</strong>
                </span>
              @endif
          </div>
        </div>

        <div class="form-group">
                <button type="submit" class="btn btn-success btn-lg">
                    {{__('Proceed')}}
                </button>
        </div>

    </form>
  </div>
</div>

@endsection
