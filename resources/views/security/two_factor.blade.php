@extends('layouts.app')

@section('content')

<div class="row">

  <div class="col-md-3">
      @include('layouts.partials.my-account-menu')
  </div>

  <div class="col-md-9">

    <div class="col-md-12">
      <div class="card">
          <div class="card-header">{{__('Two-Factor Authentication (2FA)')}}</div>

          <div class="card-body">

            @include('layouts.partials.flash-message')

            <p>
            {{__("Once configured, you'll be required to enter a code created by the Google Authenticator or Authy, in order to sign into your account.")}}
            {{__("This adds an extra layers of security making to your account.")}}
          </p>
          <p>
            @if (Auth::User()->google2fa_secret && Auth::User()->enabled_2fa)
              {{__('You already have 2FA enabled, we recommend you keep it enabled.')}}<br><br>
                <a href="{{ route('2fa.disable') }}" class="btn btn-outline-danger">{{__('Disable Two-Factor Authentication')}}</a>
            @else
              {{__('To enable 2FA click the button below.')}}<br><br>
                <a href="{{ route('2fa.enable') }}" class="btn btn-outline-success ">{{__('Enable Two-Factor Authentication')}}</a>
            @endif
          </p>
        </div>
      </div>
  </div>
</div>

@endsection
