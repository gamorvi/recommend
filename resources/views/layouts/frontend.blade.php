<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    {!! Minify::stylesheet([
                    '/css/app.css',
                    '/css/custom.css'
                ])
    !!}
</head>
<body>
    <div id="app" class="bg">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel bg-dark">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                          <li>
                          @if(Request::is('login'))
                            <a class="btn btn-outline-success my-2 my-sm-0" href="{{ route('register') }}">{{ __('Register') }}</a>
                          @else
                            <a class="btn btn-outline-success my-2 my-sm-0" href="{{ route('login') }}">{{ __('Login') }}</a>
                          @endif
                          </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                  @if (!in_array(Auth::user()->photo, ["avatar.png", NULL, '']))
                                    <img src="/storage/profile/{{Auth::user()->id}}/{{Auth::user()->photo}}" class="rounded-circle" height="28" alt="{{ Auth::user()->firstname }}">
                                  @else
                                    <img src="/storage/profile/user.png" class="rounded-circle" height="28" alt="{{ Auth::user()->firstname }}">
                                  @endif
                                  {{ Auth::user()->firstname }} <span class="caret"></span>

                                </a>
                                <ul>

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                                    <a class="dropdown-item" href="{{ route('profile') }}">
                                      {{__('My Account')}}
                                    </a>

                                    <div class="dropdown-divider"></div>

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>

                        @endguest
                    </ul>
                </div>

            </div>
        </nav>

        <main class="py-4">
          <div class="container">
            @yield('content')
          </div>
        </main>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </div>

    {!!Minify::javascript([
          '/js/app.js',
          '/js/custom.js'
          ])
    !!}
</body>
</html>
