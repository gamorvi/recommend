@if ($message = Session::get('success'))
<div class="alert alert-success alert-block animated flash">
	<button type="button" class="close" data-dismiss="alert">×</button>
        {!! $message !!}
</div>
@endif

@if ($message = Session::get('error'))
<div class="alert alert-danger alert-block animated flash">
	<button type="button" class="close" data-dismiss="alert">×</button>
        {!! $message !!}
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-block animated flash">
	<button type="button" class="close" data-dismiss="alert">×</button>
	     {!! $message !!}
</div>
@endif

@if ($message = Session::get('info'))
<div class="alert alert-info alert-block animated flash">
	<button type="button" class="close" data-dismiss="alert">×</button>
	     {!! $message !!}
</div>
@endif

@if ($errors->any())
<div class="alert alert-danger animated flash">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<i class="fa fa-exclamation-circle"></i>
	<strong>{{'Oops!'}} </strong>{{'Something went wrong'}}</br>
	@if (count($errors->all())==1)
		@foreach ($errors->all() as $error)
				{!! $error !!}
		@endforeach
	@else
		<ul>
				@foreach ($errors->all() as $error)
						<li>{!! $error !!}</li>
				@endforeach
		</ul>
	@endif
</div>
@endif
