<div class="list-group">
  <a href="{{route('profile')}}" class="list-group-item list-group-item-action
    @if (isset($profile))
      {{$profile}}
    @endif
  ">
    {{ __('Profile') }}
  </a>

  <a href="{{route('change.password')}}" class="list-group-item list-group-item-action
    @if (isset($password))
      {{$password}}
    @endif
  ">{{ __('Change Password') }}
  </a>

  <a href="{{route('two_factor')}}" class="list-group-item list-group-item-action
    @if (isset($two_factor))
      {{$two_factor}}
    @endif
  ">{{ __('Two-Factor Authentication') }}
  </a>

  <a href="{{route('access.logs')}}" class="list-group-item list-group-item-action
    @if (isset($access_logs))
      {{$access_logs}}
    @endif
  ">{{ __('Access Logs') }}</a>
  
</div>
