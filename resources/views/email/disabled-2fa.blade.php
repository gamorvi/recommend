@component('mail::message')
Hi {{ $firstname }},

Your Two-Factor Authentication on your account has been disabled.

We recommend that you keep 2FA enabled, it adds another level of security.

Click on the button below to login.

@component('mail::button', ['url' => url('/')])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
