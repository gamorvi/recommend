@component('mail::message')
Hi {{ $firstname }},

Your password was changed successfully

Click on the button below to login.

@component('mail::button', ['url' => url('/')])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
