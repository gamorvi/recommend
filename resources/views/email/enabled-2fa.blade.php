@component('mail::message')
Hi {{ $firstname }},

You have successfully activated Two-Factor Authentication (2FA) on your account.

Click on the button below to login.

@component('mail::button', ['url' => url('/')])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
