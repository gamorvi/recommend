@component('mail::message')
Hi {{ $firstname }},

Your company account was created successfully.

Click on the button below to login.

@component('mail::button', ['url' => url('/')])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
