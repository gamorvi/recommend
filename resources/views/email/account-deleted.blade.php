@component('mail::message')
Hi {{ $firstname }},

Your account was deleted successfully.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
